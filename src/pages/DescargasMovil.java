package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseDescargasMovil;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class DescargasMovil extends TestBaseDescargasMovil {
	
	final WebDriver driver;
	public DescargasMovil(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "number")
	private WebElement IngresaLineaTelefonica;
	
	@FindBy(how = How.ID,using = "acepto")
	private WebElement checkMayorDeEdad;
	
	@FindBy(how = How.ID,using = "si")
	private WebElement btnContinue;
	
	

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInDescargasMovil(String apuntaA) {		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Descargas Movil - landing");
		
		WebElement menu1 = driver.findElement(By.xpath("//a[contains(text(), 'Animales')]"));
			menu1.click();
			espera(500);
				String elemento1 = driver.findElement(By.cssSelector(".ml-2")).getText();			
					Assert.assertEquals(elemento1,"Animales");
					System.out.println(elemento1);
			espera(500);
			
		WebElement menu2 = driver.findElement(By.xpath("//a[contains(text(), 'Bebés')]"));
			menu2.click();
			espera(500);
				String elemento2 = driver.findElement(By.cssSelector(".ml-2")).getText();
					Assert.assertEquals(elemento2,"Bebés");
					System.out.println(elemento2);
			espera(500);		
					
		WebElement menu3 = driver.findElement(By.xpath("//a[contains(text(), 'Caídas')]"));
			menu3.click();
			espera(500);
				String elemento3 = driver.findElement(By.cssSelector(".ml-2")).getText();
				Assert.assertEquals(elemento3,"Caídas");
				System.out.println(elemento3);
			espera(500);
			
		WebElement menu4 = driver.findElement(By.xpath("//a[contains(text(), 'Celebrities')]"));
			menu4.click();
			espera(500);
				String elemento4 = driver.findElement(By.cssSelector(".ml-2")).getText();
				Assert.assertEquals(elemento4,"Celebrities");
				System.out.println(elemento4);
			espera(500);
			
		WebElement menu5 = driver.findElement(By.xpath("//a[contains(text(), 'Deportes')]"));
			menu5.click();
			espera(500);
				String elemento5 = driver.findElement(By.cssSelector(".ml-2")).getText();
				Assert.assertEquals(elemento5,"Deportes");
				System.out.println(elemento5);
			espera(500);
			
		WebElement menu6 = driver.findElement(By.xpath("//a[contains(text(), 'Fiestas')]"));
			menu6.click();
			espera(500);
				String elemento6 = driver.findElement(By.cssSelector(".ml-2")).getText();
				Assert.assertEquals(elemento6,"Fiestas");
				System.out.println(elemento6);
			espera(500);
			
			
		WebElement menu7 = driver.findElement(By.xpath("//a[contains(text(), 'Frases')]"));
			menu7.click();
			espera(500);
				String elemento7 = driver.findElement(By.cssSelector(".ml-2")).getText();
				Assert.assertEquals(elemento7,"Frases");
				System.out.println(elemento7);
			espera(500);
			
			WebElement menu8 = driver.findElement(By.xpath("//a[contains(text(), 'Películas')]"));
			menu8.click();
			espera(500);
				String elemento8 = driver.findElement(By.cssSelector(".ml-2")).getText();
				Assert.assertEquals(elemento8,"Películas");
				System.out.println(elemento8);
			espera(500);
			
			driver.get("http://www.descargasmovil.club/politicas-de-privacidad/");
			espera(500);
				String elemento9 = driver.findElement(By.cssSelector(".ml-2")).getText();
				Assert.assertEquals(elemento9,"Politicas de privacidad");
				System.out.println(elemento9);
			espera(500);
			
			driver.get("http://www.descargasmovil.club/terminos-y-condiciones/");
			espera(500);
				String elemento10 = driver.findElement(By.cssSelector(".ml-2")).getText();
				Assert.assertEquals(elemento10,"Terminos y condiciones");
				System.out.println(elemento10);
			espera(500);
				
		System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Descargas Movil"); 
		System.out.println();
		System.out.println("Fin de Test Patitas Descargas Movil - Landing");
			
	}			
	
}  

